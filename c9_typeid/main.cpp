#include <iostream>
#include "square.h"
#include "rectangle.h"
#include "shape.h"
using namespace std;

void draw_shape(Shape *s)
{
    s->draw();
}

// Shape -> Rectangle -> Square
// draw()
int main(int argc, char *argv[])
{
    cout << "typeid(float) : " << typeid(float).name() << endl; // msvc float 不同编译器返回不同
    cout << "typeid(int) : " << typeid(int).name() << endl;
    if (typeid(1) == typeid(int))
    {
        cout << "1 is a int " << endl;
    }
    else
    {
        cout << "1 is not a int" << endl;
    }
    Rectangle r1(1.0, 3.0, "Rectangle1");
    Shape *shape_ptr = &r1;
    Shape &shape_ref = r1;
    cout << "typeid ptr: " << typeid(shape_ptr).name() << endl;
    cout << "typeid ref: " << typeid(shape_ref).name() << endl;
    cout << "typeid *ptr: " << typeid(*shape_ptr).name() << endl;

    cout << "----- yz ------" << endl;
    return 0;
}