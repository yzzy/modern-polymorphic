#include <iostream>
#include "square.h"
#include "rectangle.h"
#include "shape.h"
using namespace std;

// Shape -> Rectangle -> Square
// draw()
int main(int argc, char *argv[])
{
    // 静态绑定的不足
    Shape s1("Shape1");
    Rectangle r1(1.0, 2.0, "Rectangle1");
    Square sq1(3.0, "Square1");
    cout << "--------------" << endl;
    // Base pointers
    Shape *shape_ptr = &s1;
    shape_ptr->draw();
    shape_ptr = &r1;
    shape_ptr->draw();
    shape_ptr = &sq1;
    shape_ptr->draw();

    cout << "----- yz ------" << endl;
    return 0;
}