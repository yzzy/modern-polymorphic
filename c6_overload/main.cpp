#include <iostream>
#include "square.h"
#include "rectangle.h"
#include "shape.h"
using namespace std;

void draw_shape(Shape *s)
{
    s->draw();
}

// Shape -> Rectangle -> Square
// draw()
int main(int argc, char *argv[])
{
    // 静态绑定的不足
    Shape s1("Shape1");
    Rectangle r1(1.0, 2.0, "Rectangle1");
    Square sq1(3.0, "Square1");
    cout << "--------------" << endl;

    Shape *shape_ptr = &r1;
    shape_ptr->draw();
    // 父类
    shape_ptr->draw("red");
    // shape_ptr->draw("red", 2);

    cout << "----- yz ------" << endl;
    return 0;
}